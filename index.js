const list = document.getElementById('listShow')
const post = document.querySelector('.content__post')
const icon = document.querySelector('.nav__icon')
const search = document.querySelector('.nav__search')
const input = document.querySelector('.nav__search')
const btnTop = document.querySelector('#scrollTop')
const url = "https://content.guardianapis.com/search?q=trending&show-tags=all&page-size=6&show-fields=all&order-by=relevance&api-key=5ef33414-1934-47dc-9892-5d09ab7c00da"
const menu = document.querySelector('nav__burger')
const data = JSON.parse(localStorage.getItem('posts'))
let tempArr = [];

search.addEventListener('keydown', function (e) {
    if (e.keyCode != 13) {
        return
    } else {
        fetchData()
    }
})
icon.addEventListener('click', function (e) {
    e.preventDefault()
    fetchData()
})

function render(response) {
    const { results } = response.response;
    for (let key in results) {
        localStorage.setItem('posts', JSON.stringify(tempArr))
        createCard(results[key].fields)
        tempArr.push(results[key].fields.headline)
    }
    return results
}

document.addEventListener('DOMContentLoaded', function () {
    fetch(url)
        .then(response => response.json())
        .then(response => {
            render(response)
        })
        .catch(error => {
            console.log(error)
        })

    window.addEventListener('scroll', function () {
        if (scrollY > 30) {
            btnTop.classList.add('show')
        } else {
            btnTop.classList.remove('show')
        }
    });

    btnTop.onclick = function (click) {
        click.preventDefault()
        scrollToTop(0, 400)
    }
});


function createList(header) {
    post.insertAdjacentHTML('afterbegin',
        `<li class="content__list"><a href="#" class="content__link">${header}</a>`)
}

function moveMenu(item) {
    item.classList.toggle("moveMenu");
    list.style.visibility = list.style.visibility === 'visible' ? 'hidden' : 'visible';
}

function autoLayoutKeyboard(str) {
    const replacer = {
        "q": "й", "w": "ц", "e": "у", "r": "к", "t": "е", "y": "н", "u": "г",
        "i": "ш", "o": "щ", "p": "з", "[": "х", "]": "ъ", "a": "ф", "s": "ы",
        "d": "в", "f": "а", "g": "п", "h": "р", "j": "о", "k": "л", "l": "д",
        ";": "ж", "'": "э", "z": "я", "x": "ч", "c": "с", "v": "м", "b": "и",
        "n": "т", "m": "ь", ",": "б", ".": "ю", "/": "."
    };

    str.replace(/[A-z/,.;\'\]\[]/g, function (x) {
        return x == x.toLowerCase() ? replacer[x] : replacer[x.toLowerCase()].toUpperCase();
    });
}

document.addEventListener('click', function (e) {
    if (e.target.tagName === 'A') {
        //   console.log(e.target.textContent)
        while (post.firstChild) {
            post.removeChild(post.firstChild);
        }

    };
})

function createCard(results) {
    const {
        byline,
        thumbnail,
        standfirst,
        firstPublicationDate
    } = results;

    const div = document.createElement('div');
    const img = document.createElement('img');
    const p = document.createElement('p');
    const span = document.createElement('span');
    const h = document.createElement('h4')

    img.setAttribute("src", thumbnail);
    img.setAttribute("alt", standfirst);
    h.innerHTML = byline;
    p.innerHTML = standfirst;
    span.innerHTML = firstPublicationDate;
    post.appendChild(div).appendChild(img);
    post.appendChild(div).appendChild(h);
    post.appendChild(div).appendChild(p).appendChild(span)
}

async function fetchData() {
    await fetch(`https://content.guardianapis.com/search?q=${autoLayoutKeyboard(input.value)}&show-tags=all&page-size=6&show-fields=all&order-by=relevance&api-key=5ef33414-1934-47dc-9892-5d09ab7c00da`)
        .then(response => {
            if (!response.ok) {
                throw Error('Error')
            }
            return response.json();
        })
        .then(response => {
            const { results } = response.response;
            if (results.length === 0) {
                post.insertAdjacentHTML('beforebegin', '<p>No exact matches found</p>')
            } else {
                for (let key in results) {
                    tempArr.push(results[key].fields.headline)
                    createList(tempArr[key]);
                }
            }
        })
        .catch(error => {
            console.log(error)
        })
}

const scrollToTop = (to, duration = 700) => {
    const
        element = document.scrollingElement || document.documentElement,
        start = element.scrollTop,
        change = to - start,
        startDate = +new Date(),
        // t = current time
        // b = start value
        // c = change in value
        // d = duration
        easeInOutQuad = function (t, b, c, d) {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t + b;
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        },
        animateScroll = function () {
            const currentDate = +new Date();
            const currentTime = currentDate - startDate;
            element.scrollTop = parseInt(easeInOutQuad(currentTime, start, change, duration));
            if (currentTime < duration) {
                requestAnimationFrame(animateScroll);
            }
            else {
                element.scrollTop = to;
            }
        };
    animateScroll();
}

